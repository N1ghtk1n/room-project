#include <windows.h>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <fstream>
#include <sstream>
#include <cmath>
#include <wchar.h>
#include <shlobj.h>
#include "sqlite3.h"


/*
	file.open("file.txt");
	file<<version<<" \n";
	file.close();
*/
bool isDrawing = false, lightTheme = true, isFullVersion = false,
		isSaved = true, isAnimate = false, isClear = true, isColorRandom = false;
std::ofstream file,file1;
sqlite3 *db = 0;
sqlite3_stmt *stmtU;
sf::RenderWindow sfmlwnd[4];
	HFONT font = CreateFont(20,10,0,0,
		FW_NORMAL,0,0,0,
		DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,
		FF_DONTCARE,"Courier New");
		
	HFONT resultFont = CreateFont(24,10,0,0,
		FW_BOLD,0,0,0,
		DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,
		FF_DONTCARE,"Courier New");
		
	HFONT bigFont = CreateFont(50,15,0,0,
		FW_BOLD,0,0,0,
		DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,
		FF_DONTCARE,"Arial");
		
	HFONT welcomeFont = CreateFont(90,15,0,0,
		FW_BOLD,0,0,0,
		DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,
		FF_DONTCARE,"Arial");
static HWND hWallpapersLength,hWallpapersHeight,hWallpapersText,
			hWallText,hWallLength,hWallHeight,hWallWidth,hSumText,hAnimationText,
			hLogInText,hSignUpText,hWelcomeText,hLogInEditText,hSignUpEditText,
			hLogInPasswordText,hSignUpPasswordText,hPercentsText,hRollsText,
			hRedText,hGreenText,hBlueText,hColorText;
static HWND hDisplay[4],hWall[4];
static HWND hAnimateBtn,hSaveBtn,hLoadBtn,hExitBtn,hThemeBtn,hSumBtn,
			hLogInBtn,hSignUpBtn,hPresetsBtn,hWallpapersButton,
			hSaveResBtn,hSaveSettsBtn,hResetBtn,hRandomColorBtn;
static HWND hWallpapersEdit,hWallpapersEdit2,
			hWallEdit,hWallEdit2,hWallEdit3,hSumEdit,hRollsEdit,
			hLoginEdit,hSignUpEdit,hLogInPasswordEdit,hSignUpPasswordEdit,
			hRedEdit,hGreenEdit,hBlueEdit;
float wallpapersLength,wallpapersHeight,wallpaperRolls,
		wallLength,wallHeight,wallWidth;
int presetNumber = 1, colors[3] = {0,0,0};
std::string userLogin, light = "light", dark = "dark";
char winValues[7][12];

void randomizeColor(){
	if (isColorRandom){
		isColorRandom = false;
		SetWindowTextA(hRandomColorBtn,"������");
	} else {
		isColorRandom = true;
		SetWindowTextA(hRandomColorBtn,"������");
	}
}

void clearWindows(){
	bool isEmpty = false;
	if (isClear){
		for (int i = 0; i < 7; i++){
			memset(winValues[i],0,strlen(winValues[i]));
		}
		GetWindowText(hWallpapersEdit, winValues[0], 12);
		GetWindowText(hWallpapersEdit2, winValues[1], 12);
		GetWindowText(hWallEdit, winValues[2], 12);
		GetWindowText(hWallEdit2, winValues[3], 12);
		GetWindowText(hWallEdit3, winValues[4], 12);
		GetWindowText(hSumEdit, winValues[5], 12);
		GetWindowText(hRollsEdit, winValues[6], 12);
		for (int i = 0; i < 7; i++){
			if (strlen(winValues[i]) != 0){
				isEmpty = true;
				break;
			}
		}
		if (!isEmpty)
			return;
		SetWindowTextA(hResetBtn,"�������");
	} else
		SetWindowTextA(hResetBtn,"��������");
	SetWindowText(hWallpapersEdit, isClear ? "" : winValues[0]);
	SetWindowText(hWallpapersEdit2, isClear ? "" : winValues[1]);
	SetWindowText(hWallEdit, isClear ? "" : winValues[2]);
	SetWindowText(hWallEdit2, isClear ? "" : winValues[3]);
	SetWindowText(hWallEdit3, isClear ? "" : winValues[4]);
	SetWindowText(hSumEdit, isClear ? "" : winValues[5]);
	SetWindowText(hRollsEdit, isClear ? "" : winValues[6]);
	isClear = !isClear ? true : false;
}

std::string editFolderPath(std::string strPath){
	for (int i = 0; i < strPath.length(); i++){
		if (strPath[i] == '\\'){
			strPath.insert(i,"\\");
			i++;
		}
	}
	return strPath;
}

void saveResult(){
	char params[7][12];
	std::string strParams[7];
	char paramNames[7][64] =
		{
			{"����� �����: "},
			{"������ �����: "},
			{"����� �����: "},
			{"������ �����: "},
			{"������ �����: "},
			{"������� ��������: "},
			{"���������� �������: "},
		};
	
	char wpFolder[MAX_PATH];
	DWORD dwFolder = MAX_PATH;
	RegGetValueA(HKEY_CURRENT_USER, TEXT("Software\\Room project\\Settings"), TEXT("UserFolder"), RRF_RT_REG_SZ, NULL, wpFolder, (LPDWORD)&dwFolder);
	
	GetWindowText(hWallpapersEdit, params[0], 12);
	GetWindowText(hWallpapersEdit2, params[1], 12);
	GetWindowText(hWallEdit, params[2], 12);
	GetWindowText(hWallEdit2, params[3], 12);
	GetWindowText(hWallEdit3, params[4], 12);
	GetWindowText(hSumEdit, params[5], 12);
	GetWindowText(hRollsEdit, params[6], 12);
	for (int i = 0; i < 7; i++){
		for (int j = 0; j < strlen(params[i]); j++){
			if (params[i][j] == '.')
				params[i][j] = ',';
		}
		strParams[i] = params[i];
	}
		
	std::string fileName = wpFolder;
	fileName += "\\";
	fileName += strParams[0] + "x" + strParams[1] + ";" + strParams[2] + "x" + strParams[3] + "x" + strParams[4] + ".txt";
	fileName = editFolderPath(fileName);
	
	file.open(fileName.c_str());
	for (int i = 0; i < 7; i++){
		file<<paramNames[i]<<strParams[i]<<" \n";
	}
	file.close();
}

static int CALLBACK BrowseCallbackProc(HWND hwnd, UINT Message, LPARAM lParam, LPARAM lpData){
    if(Message == BFFM_INITIALIZED){
        SendMessage(hwnd, BFFM_SETSELECTION, true, lpData);
    }
    return 0;
}

void browseFolder(){
    char path[MAX_PATH];
    char wpPath[MAX_PATH];
	DWORD dwPath = MAX_PATH;
	
	RegGetValueA(HKEY_CURRENT_USER, TEXT("Software\\Room project\\Settings"), TEXT("UserFolder"), RRF_RT_REG_SZ, NULL, wpPath, (LPDWORD)&dwPath);
	
    BROWSEINFO brInfo = {0};
    brInfo.lpszTitle = ("�������� �����");
    brInfo.ulFlags = BIF_RETURNONLYFSDIRS | BIF_NEWDIALOGSTYLE;
    brInfo.lpfn = BrowseCallbackProc;
    brInfo.lParam = (LPARAM)wpPath;

    LPITEMIDLIST pidl = SHBrowseForFolder(&brInfo);

    if (pidl != 0){
        SHGetPathFromIDList (pidl, path);

        IMalloc *imalloc = 0;
        if (SUCCEEDED(SHGetMalloc(&imalloc))){
            imalloc->Free(pidl);
            imalloc->Release();
        }
		
		HKEY hkey;
		if(RegCreateKeyEx(HKEY_CURRENT_USER, TEXT("Software\\Room project\\Settings"), 0, NULL, 0, KEY_WRITE, NULL, &hkey, &dwPath) == ERROR_SUCCESS){
			RegSetValueEx(hkey, "UserFolder", 0, REG_SZ, (BYTE*)&path, strlen(path));
		}
		RegCloseKey(hkey);
    }

}

void checkAnimate(){
	char wpAnimate[3];
	DWORD dwAnimate = 3;
	
	RegGetValueA(HKEY_CURRENT_USER, TEXT("Software\\Room project\\Settings"), TEXT("Animate"), RRF_RT_REG_SZ, NULL, wpAnimate, (LPDWORD)&dwAnimate);
	if (wpAnimate[0] == 'o' && wpAnimate[1] == 'n'){
		SetWindowTextA(hAnimateBtn,"���");
		isAnimate = true;
	}
	else {
		SetWindowTextA(hAnimateBtn,"����");
		isAnimate = false;
	}
}

void changeAnimate(){
	char wpAnimate[3];
	int cbMemory = 0;
	HKEY hkey;
	
	if (!isAnimate){
		SetWindowTextA(hAnimateBtn,"���");
		wpAnimate[0] = 'o';
		wpAnimate[1] = 'n';
		cbMemory = 2;
		isAnimate = true;
	} else {
		SetWindowTextA(hAnimateBtn,"����");
		wpAnimate[0] = 'o';
		wpAnimate[1] = 'f';
		wpAnimate[2] = 'f';
		cbMemory = 3;
		isAnimate = false;
	}
	DWORD dwAnimate = 3;
	
	if(RegCreateKeyEx(HKEY_CURRENT_USER, TEXT("Software\\Room project\\Settings"), 0, NULL, 0, KEY_WRITE, NULL, &hkey, &dwAnimate) == ERROR_SUCCESS){
		RegSetValueEx(hkey, "Animate", 0, REG_SZ, (BYTE*)&wpAnimate, cbMemory);
	}
	
	RegCloseKey(hkey);
}

void changeColor(){
	char wpColors[3][4];
	GetWindowText(hRedEdit, wpColors[0], 4);
	GetWindowText(hGreenEdit, wpColors[1], 4);
	GetWindowText(hBlueEdit, wpColors[2], 4);
	bool isTens = false, isHundreds = false;
	colors[0] = colors[1] = colors[2] = 0;
	
	HKEY hkey;
	DWORD dwColor = strlen(wpColors[0]);
	if(RegCreateKeyEx(HKEY_CURRENT_USER, TEXT("Software\\Room project\\Settings"), 0, NULL, 0, KEY_WRITE, NULL, &hkey, &dwColor) == ERROR_SUCCESS){
		RegSetValueEx(hkey, "colorR", 0, REG_SZ, (BYTE*)&wpColors[0], strlen(wpColors[0]));
	}
	if(RegCreateKeyEx(HKEY_CURRENT_USER, TEXT("Software\\Room project\\Settings"), 0, NULL, 0, KEY_WRITE, NULL, &hkey, &dwColor) == ERROR_SUCCESS){
		RegSetValueEx(hkey, "colorG", 0, REG_SZ, (BYTE*)&wpColors[1], strlen(wpColors[1]));
	}
	if(RegCreateKeyEx(HKEY_CURRENT_USER, TEXT("Software\\Room project\\Settings"), 0, NULL, 0, KEY_WRITE, NULL, &hkey, &dwColor) == ERROR_SUCCESS){
		RegSetValueEx(hkey, "colorB", 0, REG_SZ, (BYTE*)&wpColors[2], strlen(wpColors[2]));
	}
	RegCloseKey(hkey);
	
	for (int i = 0; i < 3; i++){
		for (int j = strlen(wpColors[i])-1; j >= 0; j--){
			if (j > 2 || !std::isdigit(wpColors[i][j]))
				continue;
			else {
				colors[i] += ((isHundreds) ? (wpColors[i][j]-'0')*100 : ((isTens) ? (wpColors[i][j]-'0')*10 : (wpColors[i][j]-'0')));
				if (!isHundreds && isTens)
					isHundreds = true;
				if (!isTens)
					isTens = true;
				
			}
		}
		if (colors[i] > 255)
			colors[i] = 255;
		isTens = isHundreds = false;
	}
}

void takeColor(){
	colors[0] = colors[1] = colors[2] = 0;
	bool isTens = false, isHundreds = false;
	char wpColor[3][64];
	DWORD dwColorR = 100,dwColorG = 100,dwColorB = 100;
	
	RegGetValueA(HKEY_CURRENT_USER, TEXT("Software\\Room project\\Settings"), TEXT("colorR"), RRF_RT_REG_SZ, NULL, wpColor[0], (LPDWORD)&dwColorR);
	RegGetValueA(HKEY_CURRENT_USER, TEXT("Software\\Room project\\Settings"), TEXT("colorG"), RRF_RT_REG_SZ, NULL, wpColor[1], (LPDWORD)&dwColorG);
	RegGetValueA(HKEY_CURRENT_USER, TEXT("Software\\Room project\\Settings"), TEXT("colorB"), RRF_RT_REG_SZ, NULL, wpColor[2], (LPDWORD)&dwColorB);
	
	for (int i = 0; i < 3; i++){
		for (int j = strlen(wpColor[i])-1; j >= 0; j--){
			if (j > 2 || !std::isdigit(wpColor[i][j]))
				continue;
			else {
				colors[i] += ((isHundreds) ? (wpColor[i][j]-'0')*100 : ((isTens) ? (wpColor[i][j]-'0')*10 : (wpColor[i][j]-'0')));
				if (!isHundreds && isTens)
					isHundreds = true;
				if (!isTens)
					isTens = true;
				
			}
		}
		isTens = isHundreds = false;
		if (colors[i] > 255)
			colors[i] = 255;
	}
	std::ostringstream ssRed, ssGreen, ssBlue;
	ssRed << colors[0];
	ssGreen << colors[1];
	ssBlue << colors[2];
	std::string strRed(ssRed.str()),strGreen(ssGreen.str()),strBlue(ssBlue.str());
	SetWindowTextA(hRedEdit,(LPCSTR)strRed.c_str());
	SetWindowTextA(hGreenEdit,(LPCSTR)strGreen.c_str());
	SetWindowTextA(hBlueEdit,(LPCSTR)strBlue.c_str());
}

void renderPreset(){
	bool isFound = true;
	char wpL[12], wpH[12], wL[12], wW[12], wH[12];
	DWORD dwBuffer = 12;
	char *strPreset;
	if (presetNumber == 1){
		strPreset = "Software\\Room project\\Presets\\First";
		presetNumber++;
	} else if (presetNumber == 2){
		strPreset = "Software\\Room project\\Presets\\Second";
		presetNumber++;
	} else if (presetNumber == 3){
		strPreset = "Software\\Room project\\Presets\\Third";
		presetNumber++;
	} else if (presetNumber == 4){
		strPreset = "Software\\Room project\\Presets\\Fourth";
		presetNumber = 1;
	}
	
	//wallpapersLength
	if(RegGetValueA(HKEY_CURRENT_USER, TEXT(strPreset), TEXT("WallpaperLength"), RRF_RT_REG_SZ, NULL, wpL, (LPDWORD)&dwBuffer) == ERROR_SUCCESS){
		if (strlen(wpL)>0){
			SetWindowText(hWallpapersEdit, wpL);
		}
	}
	if(RegGetValueA(HKEY_CURRENT_USER, TEXT(strPreset), TEXT("WallpaperHeight"), RRF_RT_REG_SZ, NULL, wpH, (LPDWORD)&dwBuffer) == ERROR_SUCCESS){
		if (strlen(wpH)>0){
			SetWindowText(hWallpapersEdit2, wpH);
		}
	}
	//wallLength
	if(RegGetValueA(HKEY_CURRENT_USER, TEXT(strPreset), TEXT("WallLength"), RRF_RT_REG_SZ, NULL, wL, (LPDWORD)&dwBuffer) == ERROR_SUCCESS){
		if (strlen(wL)>0){
			SetWindowText(hWallEdit, wL);
		}
	}
	//wallHeight
	if(RegGetValueA(HKEY_CURRENT_USER, TEXT(strPreset), TEXT("WallHeight"), RRF_RT_REG_SZ, NULL, wH, (LPDWORD)&dwBuffer) == ERROR_SUCCESS){
		if (strlen(wH)>0){
			SetWindowText(hWallEdit2, wH);
		}
	}
	//wallWidth
	if(RegGetValueA(HKEY_CURRENT_USER, TEXT(strPreset), TEXT("WallWidth"), RRF_RT_REG_SZ, NULL, wW, (LPDWORD)&dwBuffer) == ERROR_SUCCESS){
		if (strlen(wW)>0){
			SetWindowText(hWallEdit3, wW);
		}
	}
}

void saveValuesToDB(){
	char wpLength[12], wpHeight[12], wLength[12], wWidth[12], wHeight[12], sumResult[12], wpRolls[12];
	int loginLength = userLogin.length();
	char login[loginLength + 1];
			    
	strcpy(login, userLogin.c_str());
	
	GetWindowText(hWallpapersEdit, wpLength, 12);
	GetWindowText(hWallpapersEdit2, wpHeight, 12);
	GetWindowText(hWallEdit, wLength, 12);
	GetWindowText(hWallEdit2, wHeight, 12);
	GetWindowText(hWallEdit3, wWidth, 12);
	GetWindowText(hSumEdit, sumResult, 12);
	GetWindowText(hRollsEdit, wpRolls, 12);
	sqlite3_open("db/database.db",&db);
	const char* SQLqueryUpdate= "Update users Set wpLength = ?, wpHeight = ?, wLength = ?, wWidth = ?, wHeight = ?, result = ?, wpRolls = ? Where login = ?;";
	sqlite3_prepare(db, SQLqueryUpdate, -1, &stmtU, 0);
	sqlite3_bind_text(stmtU, 1, wpLength, -1,0);
	sqlite3_bind_text(stmtU, 2, wpHeight, -1,0);
	sqlite3_bind_text(stmtU, 3, wLength, -1,0);
	sqlite3_bind_text(stmtU, 4, wWidth, -1,0);
	sqlite3_bind_text(stmtU, 5, wHeight, -1,0);
	sqlite3_bind_text(stmtU, 6, sumResult, -1,0);
	sqlite3_bind_text(stmtU, 7, wpRolls, -1,0);
	sqlite3_bind_text(stmtU, 8, login, -1,0);
	sqlite3_step(stmtU);
	sqlite3_reset(stmtU);
	sqlite3_finalize(stmtU);
	sqlite3_close(db);
	MessageBox(NULL, "������ ������� ���������!","�����!",MB_ICONINFORMATION|MB_OK);
	isSaved = true;
}

void saveValuesToRegistry(){
	HKEY hkey;
	char wpLength[12], wpHeight[12], wLength[12], wWidth[12], wHeight[12], sumResult[12], wallpRolls[12];
	DWORD dwSaveBuffer = 12;
	
	GetWindowText(hWallpapersEdit, wpLength, 12);
	GetWindowText(hWallpapersEdit2, wpHeight, 12);
	GetWindowText(hWallEdit, wLength, 12);
	GetWindowText(hWallEdit2, wHeight, 12);
	GetWindowText(hWallEdit3, wWidth, 12);
	GetWindowText(hSumEdit, sumResult, 12);
	GetWindowText(hRollsEdit, wallpRolls, 12);
	if (strlen(wpLength) == 0 || strlen(wpHeight) == 0 || strlen(wLength) == 0 || strlen(wHeight) == 0 || strlen(wWidth) == 0 || strlen(sumResult) == 0 || strlen(wallpRolls) == 0){
		MessageBox(NULL, "���� �� ����� ���� �������!","������!",MB_ICONEXCLAMATION|MB_OK);
		return;
	}
	
	//WallpaperLength
	DWORD dwWpl = strlen(wpLength);
	if(RegCreateKeyEx(HKEY_CURRENT_USER, TEXT("Software\\Room project\\Settings"), 0, NULL, 0, KEY_WRITE, NULL, &hkey, &dwWpl) == ERROR_SUCCESS){
		RegSetValueEx(hkey, "WallpaperLength", 0, REG_SZ, (BYTE*)&wpLength, strlen(wpLength));
	}
	
	//WallpaperHeight
	DWORD dwWph = strlen(wpHeight);
	if(RegCreateKeyEx(HKEY_CURRENT_USER, TEXT("Software\\Room project\\Settings"), 0, NULL, 0, KEY_WRITE, NULL, &hkey, &dwWph) == ERROR_SUCCESS){
		RegSetValueEx(hkey, "WallpaperHeight", 0, REG_SZ, (BYTE*)&wpHeight, strlen(wpHeight));
	}
	//WallLength
	DWORD dwWl = strlen(wLength);
	if(RegCreateKeyEx(HKEY_CURRENT_USER, TEXT("Software\\Room project\\Settings"), 0, NULL, 0, KEY_WRITE, NULL, &hkey, &dwWl) == ERROR_SUCCESS){
		RegSetValueEx(hkey, "WallLength", 0, REG_SZ, (BYTE*)&wLength, strlen(wLength));
	}
	//WallHeight
	DWORD dwWh = strlen(wHeight);
	if(RegCreateKeyEx(HKEY_CURRENT_USER, TEXT("Software\\Room project\\Settings"), 0, NULL, 0, KEY_WRITE, NULL, &hkey, &dwWh) == ERROR_SUCCESS){
		RegSetValueEx(hkey, "WallHeight", 0, REG_SZ, (BYTE*)&wHeight, strlen(wHeight));
	}
	//WallWidth
	DWORD dwWw = strlen(wWidth);
	if(RegCreateKeyEx(HKEY_CURRENT_USER, TEXT("Software\\Room project\\Settings"), 0, NULL, 0, KEY_WRITE, NULL, &hkey, &dwWw) == ERROR_SUCCESS){
		RegSetValueEx(hkey, "WallWidth", 0, REG_SZ, (BYTE*)&wWidth, strlen(wWidth));
	}
	//Result
	DWORD dwRes = strlen(sumResult);
	if(RegCreateKeyEx(HKEY_CURRENT_USER, TEXT("Software\\Room project\\Settings"), 0, NULL, 0, KEY_WRITE, NULL, &hkey, &dwRes) == ERROR_SUCCESS){
		RegSetValueEx(hkey, "Result", 0, REG_SZ, (BYTE*)&sumResult, strlen(sumResult));
	}
	//Rolls
	DWORD dwRolls = strlen(wallpRolls);
	if(RegCreateKeyEx(HKEY_CURRENT_USER, TEXT("Software\\Room project\\Settings"), 0, NULL, 0, KEY_WRITE, NULL, &hkey, &dwRolls) == ERROR_SUCCESS){
		RegSetValueEx(hkey, "Rolls", 0, REG_SZ, (BYTE*)&wallpRolls, strlen(wallpRolls));
	}
	
	RegCloseKey(hkey);
	MessageBox(NULL, "������ ������� ���������!","�����!",MB_ICONINFORMATION|MB_OK);
	isSaved = true;
}

void changeTheme(HWND hwnd, bool newWnd){
	char cTheme[12];
	DWORD dwBuffer = 12;
	
	if(RegGetValueA(HKEY_CURRENT_USER, TEXT("Software\\Room project\\Settings"), TEXT("Theme"), RRF_RT_REG_SZ, NULL, cTheme, (LPDWORD)&dwBuffer) == ERROR_SUCCESS){
		if (cTheme[0]=='d' && cTheme[1]=='a' && cTheme[2]=='r' && cTheme[3]=='k'){
			lightTheme = newWnd ? false : true;
			if (!newWnd){
				DWORD dwLight = light.length();
			    char cLight[light.length() + 1];
			    strcpy(cLight, light.c_str());
				HKEY hkey;
				if(RegCreateKeyEx(HKEY_CURRENT_USER, TEXT("Software\\Room project\\Settings"), 0, NULL, 0, KEY_WRITE, NULL, &hkey, &dwLight) == ERROR_SUCCESS){
					RegSetValueEx(hkey, "Theme", 0, REG_SZ, (BYTE*)&cLight, strlen(cLight));
				} else
					MessageBox(NULL, "���� �� ���������! ������ � �������.","������!",MB_ICONEXCLAMATION|MB_OK);
				RegCloseKey(hkey);
			}
		} else {
			lightTheme = newWnd ? true : false;
			if (!newWnd){
				DWORD dwDark = dark.length();
				char cDark[dark.length() + 1];
			    strcpy(cDark, dark.c_str());
				HKEY hkey;
				if(RegCreateKeyEx(HKEY_CURRENT_USER, TEXT("Software\\Room project\\Settings"), 0, NULL, 0, KEY_WRITE, NULL, &hkey, &dwDark) == ERROR_SUCCESS){
					RegSetValueEx(hkey, "Theme", 0, REG_SZ, (BYTE*)&cDark, strlen(cDark));
				} else
					MessageBox(NULL, "���� �� ���������! ������ � �������.","������!",MB_ICONEXCLAMATION|MB_OK);
				RegCloseKey(hkey);
			}
		}
	} else {
		MessageBox(NULL, "��������� ������ �� �����������!","������!",MB_ICONEXCLAMATION|MB_OK);
		lightTheme = true;
	}
	
	
	HBRUSH brush = CreateSolidBrush(RGB(0, 255, 255));
    SetClassLongPtr(hwnd, GCLP_HBRBACKGROUND, (LONG)brush);
    DeleteObject((HGDIOBJ)brush);
    //Repaint
	InvalidateRect(hwnd, NULL, TRUE);
	
	SendMessage(hwnd, WM_PAINT, 0, (LPARAM)"fill");
}

bool checkIsFullVersion(){
	HKEY hkey;
	bool isFull = true;
	char version[6];
	DWORD dwDisposition, dwBuffer = 6;
	
	if(RegGetValueA(HKEY_CURRENT_USER, TEXT("Software\\Room project\\Settings"), TEXT("FullVersion"), RRF_RT_REG_SZ, NULL, version, (LPDWORD)&dwBuffer) == ERROR_SUCCESS){
		if (version[0]=='0'){
			isFull = false;
		}
	} else
		MessageBox(NULL, "��������� ������ �� �����������!","������!",MB_ICONEXCLAMATION|MB_OK);
	RegCloseKey(hkey);
	return isFull;
}

float editTextToNumber(char *editText){
	float value;
	std::string strNumber(editText); 
	std::stringstream ssNumber(strNumber);
	ssNumber >> value;
	return value;
}

bool checkParams(float wpL,float wpH,float wL,float wH,float wW){
	if (wpL == 0){
		MessageBox(NULL, "����� ����� �� ����� ���� ����� 0!","������!",MB_ICONEXCLAMATION|MB_OK);
		return false;
	} else if (wpH == 0){
		MessageBox(NULL, "������ ����� �� ����� ���� ����� 0!","������!",MB_ICONEXCLAMATION|MB_OK);
		return false;
	} else if (wL == 0){
		MessageBox(NULL, "����� ����� �� ����� ���� ����� 0!","������!",MB_ICONEXCLAMATION|MB_OK);
		return false;
	} else if (wW == 0){
		MessageBox(NULL, "������ ����� �� ����� ���� ����� 0!","������!",MB_ICONEXCLAMATION|MB_OK);
		return false;
	} else if (wH == 0){
		MessageBox(NULL, "������ ����� �� ����� ���� ����� 0!","������!",MB_ICONEXCLAMATION|MB_OK);
		return false;
	} else if (wpH < wH){
		MessageBox(NULL, "������ ����� �� ����� ���� ������ ������ �����!","������!",MB_ICONEXCLAMATION|MB_OK);
		return false;
	} else return true;
}

bool checkEdit(char *editText){
	bool isNumber = true, isPoint = false;
	int textLength = strlen(editText);
	if (textLength == 0){
		MessageBox(NULL, "���� �� ������ ���� �������!","������!",MB_ICONEXCLAMATION|MB_OK);
		return false;
	}
	for (int i = 0; i < textLength; i++){
		if (editText[i] == '.' || editText[i] == ','){
			if (isPoint){
				isNumber = false;
				MessageBox(NULL, "����� �������������� ������ ���� �����!","������!",MB_ICONEXCLAMATION|MB_OK);
				break;
			}
			isPoint = true;
			editText[i] = '.';
		}
		if (!isdigit(editText[i]) && (editText[i] != '.' && i != 0)){
			isNumber = false;
			MessageBox(NULL, "������� �������� ��������!","������!",MB_ICONEXCLAMATION|MB_OK);
			break;
		}
	}
	return isNumber;
}

void calculatePercent(float wpL,float wpH,float wL,float wW,float wH){
	int koef = 1;
	float wpHRemainder = 0;
	float roomLength = (wL*2) + (wW*2);
	if (wpH > (2*wH)){
		koef = wpH/wH;
		wpHRemainder = wpH - (wH*koef);
	}
	wallpaperRolls = roomLength/(wpL*koef);
	float wallpaperRollsMax = roomLength/wpL;
	int wRolls = std::floor(wallpaperRolls);
	int wRollsMax = std::floor(wallpaperRollsMax);
	if ((wRolls*wpL) < roomLength){
		wallpaperRolls = std::ceil(wallpaperRolls);
		wallpaperRollsMax = std::ceil(wallpaperRollsMax);
	}
	float roomArea = roomLength*wH;
	float wallpaperArea = (roomLength*wpHRemainder)+(((wallpaperRollsMax*wpL) - roomLength)*(wpHRemainder+wH))+roomArea;
	
	float onePercent = wallpaperArea/100;
	std::ostringstream ssRemainders, ssRolls;
	ssRemainders << ((wallpaperArea - roomArea)/onePercent);
	ssRolls << wallpaperRolls;
	std::string strPercent(ssRemainders.str()), strRolls(ssRolls.str());
	SetWindowText(hSumEdit, (LPCSTR)strPercent.c_str());
	SetWindowText(hRollsEdit, (LPCSTR)strRolls.c_str());
}

bool setParameters(char *wpL,char *wpH,char *wL,char *wH,char *wW){
	wallpapersLength = editTextToNumber(wpL);
	wallpapersHeight = editTextToNumber(wpH);
	wallLength = editTextToNumber(wL);
	wallHeight = editTextToNumber(wH);
	wallWidth = editTextToNumber(wW);
	if (checkParams(wallpapersLength,wallpapersHeight,wallLength,wallHeight,wallWidth)){
		calculatePercent(wallpapersLength,wallpapersHeight,wallLength,wallWidth,wallHeight);
		return true;
	} else
		return false;
}

void loadFromRegistry(){
	bool isFound = true;
	char wpL[12], wpH[12], wL[12], wW[12], wH[12], result[12], wpRolls[12];
	DWORD dwBuffer = 12;
		
	//wallpapersLength
	if(RegGetValueA(HKEY_CURRENT_USER, TEXT("Software\\Room project\\Settings"), TEXT("WallpaperLength"), RRF_RT_REG_SZ, NULL, wpL, (LPDWORD)&dwBuffer) == ERROR_SUCCESS){
		if (strlen(wpL)>0){
			SetWindowText(hWallpapersEdit, wpL);
		} else
			isFound = false;
	} else
		isFound = false;
	//wallpapersHeight
	if(RegGetValueA(HKEY_CURRENT_USER, TEXT("Software\\Room project\\Settings"), TEXT("WallpaperHeight"), RRF_RT_REG_SZ, NULL, wpH, (LPDWORD)&dwBuffer) == ERROR_SUCCESS){
		if (strlen(wpH)>0){
			SetWindowText(hWallpapersEdit2, wpH);
		} else
			isFound = false;
	} else
		isFound = false;
	//wallLength
	if(RegGetValueA(HKEY_CURRENT_USER, TEXT("Software\\Room project\\Settings"), TEXT("WallLength"), RRF_RT_REG_SZ, NULL, wL, (LPDWORD)&dwBuffer) == ERROR_SUCCESS){
		if (strlen(wL)>0){
			SetWindowText(hWallEdit, wL);
		} else
			isFound = false;
	} else
		isFound = false;
	//wallHeight
	if(RegGetValueA(HKEY_CURRENT_USER, TEXT("Software\\Room project\\Settings"), TEXT("WallHeight"), RRF_RT_REG_SZ, NULL, wH, (LPDWORD)&dwBuffer) == ERROR_SUCCESS){
		if (strlen(wH)>0){
			SetWindowText(hWallEdit2, wH);
		} else
			isFound = false;
	} else
		isFound = false;
	//wallWidth
	if(RegGetValueA(HKEY_CURRENT_USER, TEXT("Software\\Room project\\Settings"), TEXT("WallWidth"), RRF_RT_REG_SZ, NULL, wW, (LPDWORD)&dwBuffer) == ERROR_SUCCESS){
		if (strlen(wW)>0){
			SetWindowText(hWallEdit3, wW);
		} else
			isFound = false;
	} else
		isFound = false;
	//result
	if(RegGetValueA(HKEY_CURRENT_USER, TEXT("Software\\Room project\\Settings"), TEXT("Result"), RRF_RT_REG_SZ, NULL, result, (LPDWORD)&dwBuffer) == ERROR_SUCCESS){
		if (strlen(result)>0){
			SetWindowText(hSumEdit, result);
		} else
			isFound = false;
	} else
		isFound = false;
	//rolls
	if(RegGetValueA(HKEY_CURRENT_USER, TEXT("Software\\Room project\\Settings"), TEXT("Rolls"), RRF_RT_REG_SZ, NULL, wpRolls, (LPDWORD)&dwBuffer) == ERROR_SUCCESS){
		if (strlen(wpRolls)>0){
			SetWindowText(hRollsEdit, wpRolls);
		} else
			isFound = false;
	} else
		isFound = false;
	
	if (isFound){
		setParameters(wpL,wpH,wL,wH,wW);
	}
}

void loadFromDB(){
	sqlite3_open("db/database.db",&db);
	const char* SQLquery="Select * From 'users' Where login = ?;";
	char *wpL = NULL, *wpH = NULL, *wL = NULL, *wW = NULL, *wH = NULL, *sumResult = NULL, *wpRolls = NULL;
	bool isFound = false;
	int loginLength = userLogin.length();
	char login[loginLength + 1];
			    
	strcpy(login, userLogin.c_str());
				
	sqlite3_prepare(db, SQLquery, -1, &stmtU, 0);
	sqlite3_bind_text(stmtU, 1, login, -1,0);
	while (sqlite3_step(stmtU) == SQLITE_ROW){
		wpL = (char*)sqlite3_column_text(stmtU,2);
		isFound = true;
		if (wpL == "" || wpL == NULL){
			MessageBox(NULL, "� ��� ��� ����������� ������","������!",MB_ICONEXCLAMATION|MB_OK);
			break;
		}
		wpH = (char*)sqlite3_column_text(stmtU,3);
		wL = (char*)sqlite3_column_text(stmtU,4);
		wW = (char*)sqlite3_column_text(stmtU,5);
		wH = (char*)sqlite3_column_text(stmtU,6);
		sumResult = (char*)sqlite3_column_text(stmtU,7);
		wpRolls = (char*)sqlite3_column_text(stmtU,8);
		setParameters(wpL,wpH,wL,wH,wW);
		SetWindowText(hWallpapersEdit, wpL);
		SetWindowText(hWallpapersEdit2, wpH);
		SetWindowText(hWallEdit, wL);
		SetWindowText(hWallEdit2, wH);
		SetWindowText(hWallEdit3, wW);
		SetWindowText(hSumEdit, sumResult);
		SetWindowText(hRollsEdit, wpRolls);
	}
	sqlite3_reset(stmtU);
	sqlite3_finalize(stmtU);
	sqlite3_close(db);
	if (!isFound)
		MessageBox(NULL, "������������ ����� ��� ������","������!",MB_ICONEXCLAMATION|MB_OK);
}

void animate(){
	sf::Texture wallpaperTexture;
	wallpaperTexture.loadFromFile("res/wallpapers.png");
	sf::Sprite spriteFrstWpaper(wallpaperTexture),spriteSecWpaper(wallpaperTexture),
		spriteThrdWpaper(wallpaperTexture),spriteFrthWpaper(wallpaperTexture);
	
	if (!isColorRandom){
		spriteFrstWpaper.setColor(sf::Color(colors[0],colors[1],colors[2]));
		spriteSecWpaper.setColor(sf::Color(colors[0],colors[1],colors[2]));
		spriteThrdWpaper.setColor(sf::Color(colors[0],colors[1],colors[2]));
		spriteFrthWpaper.setColor(sf::Color(colors[0],colors[1],colors[2]));
	} else {
		srand(time(0));
		spriteFrstWpaper.setColor(sf::Color(0 + rand()%255,0 + rand()%255,0 + rand()%255));
		spriteSecWpaper.setColor(sf::Color(0 + rand()%255,0 + rand()%255,0 + rand()%255));
		spriteThrdWpaper.setColor(sf::Color(0 + rand()%255,0 + rand()%255,0 + rand()%255));
		spriteFrthWpaper.setColor(sf::Color(0 + rand()%255,0 + rand()%255,0 + rand()%255));
	}
	
	spriteFrstWpaper.setPosition(spriteFrstWpaper.getPosition().x,-spriteFrstWpaper.getTextureRect().height);
	spriteSecWpaper.setPosition(sfmlwnd[2].getSize().x+2+spriteThrdWpaper.getTextureRect().height,spriteSecWpaper.getPosition().y);
	spriteThrdWpaper.setPosition(sfmlwnd[3].getSize().x+2,sfmlwnd[3].getSize().y+2+spriteThrdWpaper.getTextureRect().width);
	spriteFrthWpaper.setPosition(-spriteThrdWpaper.getTextureRect().height,sfmlwnd[1].getSize().y+2);
	spriteSecWpaper.setRotation(90.0f);
	spriteThrdWpaper.setRotation(180.0f);
	spriteFrthWpaper.setRotation(-90.0f);
	
	sfmlwnd[0].clear();
	sfmlwnd[1].clear();
	sfmlwnd[2].clear();
	sfmlwnd[3].clear();
	
	float scale = 1 / ((wallLength > wallWidth) ? (wallLength/wallpapersLength) : (wallWidth/wallpapersLength));
	float scale1 = 1 / ((wallLength < wallWidth) ? (wallLength/wallpapersLength) : (wallWidth/wallpapersLength));
	spriteFrstWpaper.setScale(scale,1);
	spriteSecWpaper.setScale(scale1,1);
	spriteThrdWpaper.setScale(scale,1);
	spriteFrthWpaper.setScale(scale1,1);
	
	sf::Clock clock;
	float time = clock.getElapsedTime().asSeconds();
    clock.restart();
    float timer = 0, delay = 21.0, times = 0.007;
    int x = 0, y = 0, x2 = 2, y2 = 0, x3 = 0, y3 = -2, x4 = 0, y4 = -2;
    timer += time;
    x = spriteFrstWpaper.getPosition().x;
	y = 20;
	bool firstDisp = true, secDisp = false, thrdDisp = false, frthDisp = false;
    !isDrawing ? isDrawing = true : isDrawing = false;
    while(isDrawing){
		float time = clock.getElapsedTime().asSeconds();
    	clock.restart();
		timer += time;
		
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left)){
			isDrawing = false;
		}
		if (timer>times){
			
			//FirstDisplay
			if (spriteFrstWpaper.getPosition().y >= 0 && firstDisp){
				if ((spriteFrstWpaper.getPosition().x + (spriteFrstWpaper.getTextureRect().width*spriteFrstWpaper.getScale().x)) >= (sfmlwnd[0].getSize().x+2)){
					float dif = ((spriteFrstWpaper.getPosition().x + (spriteFrstWpaper.getTextureRect().width*spriteFrstWpaper.getScale().x))-(sfmlwnd[0].getSize().x+2))/(spriteFrstWpaper.getTextureRect().width*spriteFrstWpaper.getScale().x);
					
					spriteSecWpaper.setPosition((sfmlwnd[2].getSize().x+2)+spriteSecWpaper.getTextureRect().height,
							-((spriteSecWpaper.getTextureRect().width*spriteSecWpaper.getScale().x)-(spriteSecWpaper.getTextureRect().width*spriteSecWpaper.getScale().x*dif)));
					firstDisp = false;
					secDisp = true;
					if (isColorRandom)
						spriteSecWpaper.setColor(sf::Color(spriteFrstWpaper.getColor().r,spriteFrstWpaper.getColor().g,spriteFrstWpaper.getColor().b));
					continue;
				}
				if (isColorRandom)
					spriteFrstWpaper.setColor(sf::Color(0 + rand()%255,0 + rand()%255,0 + rand()%255));
				x = (spriteFrstWpaper.getTextureRect().width*spriteFrstWpaper.getScale().x);
				y = -spriteFrstWpaper.getTextureRect().height;
				spriteFrstWpaper.move(x,y);
				times += 0.007;
				continue;
			}
			
			//SecondDisplay
			if (spriteSecWpaper.getPosition().x <= sfmlwnd[2].getSize().x && secDisp){
				if ((spriteSecWpaper.getPosition().y + (spriteSecWpaper.getTextureRect().width*spriteSecWpaper.getScale().x)) >= (sfmlwnd[2].getSize().y+2)){
					float dif1 = ((spriteSecWpaper.getPosition().y + (spriteSecWpaper.getTextureRect().width*spriteSecWpaper.getScale().x))-(sfmlwnd[2].getSize().y+2))/(spriteSecWpaper.getTextureRect().width*spriteSecWpaper.getScale().x);
					
					spriteThrdWpaper.setPosition(((sfmlwnd[3].getSize().x+2) + ((spriteThrdWpaper.getTextureRect().width*spriteThrdWpaper.getScale().x)-(spriteThrdWpaper.getTextureRect().width*spriteThrdWpaper.getScale().x*dif1))),
						(sfmlwnd[3].getSize().x+2));
					secDisp = false;
					thrdDisp = true;
					if (isColorRandom)
						spriteThrdWpaper.setColor(sf::Color(spriteSecWpaper.getColor().r,spriteSecWpaper.getColor().g,spriteSecWpaper.getColor().b));
					continue;
				}
				if (isColorRandom)
					spriteSecWpaper.setColor(sf::Color(0 + rand()%255,0 + rand()%255,0 + rand()%255));
				x2 = sfmlwnd[2].getSize().x+2;
				y2 = (spriteSecWpaper.getTextureRect().width*spriteSecWpaper.getScale().x);
				spriteSecWpaper.move(x2,y2);
				times += 0.007;
				continue;
			}
			
			//ThirdDisplay
			if (spriteThrdWpaper.getPosition().y <= sfmlwnd[3].getSize().y && thrdDisp){
				if ((spriteThrdWpaper.getPosition().x - (spriteThrdWpaper.getTextureRect().width*spriteThrdWpaper.getScale().x)) <= 0){
					float rem = (spriteThrdWpaper.getTextureRect().width*spriteThrdWpaper.getScale().x)-((spriteThrdWpaper.getTextureRect().width*spriteThrdWpaper.getScale().x) + spriteThrdWpaper.getPosition().x);
					float dif2 = (rem)/(spriteThrdWpaper.getTextureRect().width*spriteThrdWpaper.getScale().x);
					
					spriteFrthWpaper.setPosition(-spriteFrthWpaper.getTextureRect().height,
						(sfmlwnd[1].getSize().y+2) - (spriteFrthWpaper.getTextureRect().width*spriteFrthWpaper.getScale().x*dif2));
					thrdDisp = false;
					frthDisp = true;
					if (isColorRandom)
						spriteFrthWpaper.setColor(sf::Color(spriteThrdWpaper.getColor().r,spriteThrdWpaper.getColor().g,spriteThrdWpaper.getColor().b));
					continue;
				}
				if (isColorRandom)
					spriteThrdWpaper.setColor(sf::Color(0 + rand()%255,0 + rand()%255,0 + rand()%255));
				x3 = -(spriteThrdWpaper.getTextureRect().width*spriteThrdWpaper.getScale().x);
				y3 = sfmlwnd[3].getSize().y+2;
				spriteThrdWpaper.move(x3,y3);
				times += 0.007;
				continue;
			}
			
			//FourthDisplay
			if (spriteFrthWpaper.getPosition().x >= 0 && frthDisp){
				if ((spriteFrthWpaper.getPosition().y - (spriteFrthWpaper.getTextureRect().width*spriteFrthWpaper.getScale().x)) <= 0){
					frthDisp = false;
					isDrawing = false;
					continue;
				}
				if (isColorRandom)
					spriteFrthWpaper.setColor(sf::Color(0 + rand()%255,0 + rand()%255,0 + rand()%255));
				x4 = -spriteFrthWpaper.getTextureRect().height;
				y4 = -(spriteFrthWpaper.getTextureRect().width*spriteFrthWpaper.getScale().x);
				spriteFrthWpaper.move(x4,y4);
				times += 0.007;
				continue;
			}
			
			if (firstDisp){
				x = 0;
				y = 2;
				spriteFrstWpaper.move(x,y);
			}
			if (secDisp){
				x2 = -2;
				y2 = 0;
				spriteSecWpaper.move(x2,y2);
			}
			if (thrdDisp){
				x3 = 0;
				y3 = -2;
				spriteThrdWpaper.move(x3,y3);
			}
			if (frthDisp){
				x4 = 2;
				y4 = 0;
				spriteFrthWpaper.move(x4,y4);
			}
			times += 0.007;
		}
		if (timer>delay){
			isDrawing = false;
		}
		if (firstDisp){
			sfmlwnd[0].draw(spriteFrstWpaper);
			sfmlwnd[0].display();
		} else if (secDisp){
			sfmlwnd[2].draw(spriteSecWpaper);
			sfmlwnd[2].display();
		} else	if (thrdDisp){
			sfmlwnd[3].draw(spriteThrdWpaper);
			sfmlwnd[3].display();
		} else {
			sfmlwnd[1].draw(spriteFrthWpaper);
			sfmlwnd[1].display();
		}
	}
}

void renderComponents(HWND hwnd, HINSTANCE hInst){
	HDC hdc = GetDC(hwnd);
	
	//WallpapersLength
	hWallpapersEdit = CreateWindow("edit", "",
		WS_CHILD | WS_VISIBLE | WS_BORDER | ES_LEFT, 10, 130, 100, 20,
		hwnd, 0, hInst, NULL);
	ShowWindow(hWallpapersEdit, SW_SHOWNORMAL);
	SendMessage(hWallpapersEdit,EM_SETLIMITTEXT,6,0);
	//WallpapersHeight
	hWallpapersEdit2 = CreateWindow("edit", "",
		WS_CHILD | WS_VISIBLE | WS_BORDER | ES_LEFT, 130, 130, 100, 20,
		hwnd, 0, hInst, NULL);
	ShowWindow(hWallpapersEdit2, SW_SHOWNORMAL);
	SendMessage(hWallpapersEdit2,EM_SETLIMITTEXT,6,0);
	
	
	
	//WallsLength
	hWallEdit = CreateWindow("edit", "",
		WS_CHILD | WS_VISIBLE | WS_BORDER | ES_LEFT, 10, 245, 100, 20,
		hwnd, 0, hInst, NULL);
	ShowWindow(hWallEdit, SW_SHOWNORMAL);
	SendMessage(hWallEdit,EM_SETLIMITTEXT,6,0);
	//WallsHeight
	hWallEdit2 = CreateWindow("edit", "",
		WS_CHILD | WS_VISIBLE | WS_BORDER | ES_LEFT, 130, 245, 100, 20,
		hwnd, 0, hInst, NULL);
	ShowWindow(hWallEdit2, SW_SHOWNORMAL);
	SendMessage(hWallEdit2,EM_SETLIMITTEXT,6,0);
	//WallsWidth
	hWallEdit3 = CreateWindow("edit", "",
		WS_CHILD | WS_VISIBLE | WS_BORDER | ES_LEFT, 10, 290, 100, 20,
		hwnd, 0, hInst, NULL);
	ShowWindow(hWallEdit3, SW_SHOWNORMAL);
	SendMessage(hWallEdit3,EM_SETLIMITTEXT,6,0);
	
	
	
	//WallpapersText
	hWallpapersLength = CreateWindow(
		"Static","�����",
		WS_CHILD | WS_VISIBLE,
		10, 110, 50, 20,
		hwnd, (HMENU)NULL, NULL, NULL);
	SendMessage(hWallpapersLength,WM_SETFONT,(WPARAM)font,0);
	ShowWindow(hWallpapersLength, SW_SHOWNORMAL);
	
	hWallpapersHeight = CreateWindow(
		"Static","������",
		WS_CHILD | WS_VISIBLE,
		130, 110, 60, 20,
		hwnd, (HMENU)NULL, NULL, NULL);
	SendMessage(hWallpapersHeight,WM_SETFONT,(WPARAM)font,0);
	ShowWindow(hWallpapersHeight, SW_SHOWNORMAL);
	
	hWallpapersText = CreateWindow(
		"Static","����",
		WS_CHILD | WS_VISIBLE,
		10, 55, 75, 45,
		hwnd, (HMENU)NULL, NULL, NULL);
	SendMessage(hWallpapersText,WM_SETFONT,(WPARAM)bigFont,0);
	ShowWindow(hWallpapersText, SW_SHOWNORMAL);
	
	
	//WallsText
	hWallLength = CreateWindow(
		"Static","�����",
		WS_CHILD | WS_VISIBLE,
		10, 225, 50, 20,
		hwnd, (HMENU)NULL, NULL, NULL);
	SendMessage(hWallLength,WM_SETFONT,(WPARAM)font,0);
	ShowWindow(hWallLength, SW_SHOWNORMAL);
	
	hWallHeight = CreateWindow(
		"Static","������",
		WS_CHILD | WS_VISIBLE,
		130, 225, 60, 20,
		hwnd, (HMENU)NULL, NULL, NULL);
	SendMessage(hWallHeight,WM_SETFONT,(WPARAM)font,0);
	ShowWindow(hWallHeight, SW_SHOWNORMAL);
	
	hWallWidth = CreateWindow(
		"Static","������",
		WS_CHILD | WS_VISIBLE,
		10, 270, 60, 20,
		hwnd, (HMENU)NULL, NULL, NULL);
	SendMessage(hWallWidth,WM_SETFONT,(WPARAM)font,0);
	ShowWindow(hWallWidth, SW_SHOWNORMAL);
	
	hWallText = CreateWindow(
		"Static","�������",
		WS_CHILD | WS_VISIBLE,
		10, 170, 123, 45,
		hwnd, (HMENU)NULL, NULL, NULL);
	SendMessage(hWallText,WM_SETFONT,(WPARAM)bigFont,0);
	ShowWindow(hWallText, SW_SHOWNORMAL);
	
	
	//Result
	hSumEdit = CreateWindow("edit", "",
		WS_CHILD | WS_VISIBLE | WS_BORDER | ES_LEFT | ES_READONLY, 140, 390, 80, 28,
		hwnd, 0, hInst, NULL);
	SendMessage(hSumEdit,WM_SETFONT,(WPARAM)resultFont,0);
	ShowWindow(hSumEdit, SW_SHOWNORMAL);
	
	hRollsEdit = CreateWindow("edit", "",
		WS_CHILD | WS_VISIBLE | WS_BORDER | ES_LEFT | ES_READONLY, 140, 430, 80, 28,
		hwnd, 0, hInst, NULL);
	SendMessage(hRollsEdit,WM_SETFONT,(WPARAM)resultFont,0);
	ShowWindow(hRollsEdit, SW_SHOWNORMAL);
	
	hPercentsText = CreateWindow(
		"Static","�������,%",
		WS_CHILD | WS_VISIBLE,
		30, 390, 90, 20,
		hwnd, (HMENU)NULL, NULL, NULL);
	SendMessage(hPercentsText,WM_SETFONT,(WPARAM)font,0);
	ShowWindow(hPercentsText, SW_SHOWNORMAL);
	
	hRollsText = CreateWindow(
		"Static","�������,��.",
		WS_CHILD | WS_VISIBLE,
		30, 430, 100, 20,
		hwnd, (HMENU)NULL, NULL, NULL);
	SendMessage(hRollsText,WM_SETFONT,(WPARAM)font,0);
	ShowWindow(hRollsText, SW_SHOWNORMAL);
	
	hSumText = CreateWindow(
		"Static","���������",
		WS_CHILD | WS_VISIBLE,
		48, 330, 144, 48,
		hwnd, (HMENU)NULL, NULL, NULL);
	SendMessage(hSumText,WM_SETFONT,(WPARAM)bigFont,0);
	ShowWindow(hSumText, SW_SHOWNORMAL);
	
	
	//Displays
	hDisplay[0] = CreateWindow("Static",NULL,
		WS_CHILD | WS_VISIBLE | WS_BORDER,
		255, 60, 300, 110,
		hwnd, (HMENU)NULL, NULL, NULL);
	sfmlwnd[0].create(hDisplay[0]);
	
	hDisplay[1] = CreateWindow("Static",NULL,
		WS_CHILD | WS_VISIBLE | WS_BORDER,
		255, 180, 110, 300,
		hwnd, (HMENU)NULL, NULL, NULL);
	sfmlwnd[1].create(hDisplay[1]);
	
	hDisplay[2] = CreateWindow("Static",NULL,
		WS_CHILD | WS_VISIBLE | WS_BORDER,
		445, 180, 110, 300,
		hwnd, (HMENU)NULL, NULL, NULL);
	sfmlwnd[2].create(hDisplay[2]);
	
	hDisplay[3] = CreateWindow("Static",NULL,
		WS_CHILD | WS_VISIBLE | WS_BORDER,
		255, 490, 300, 110,
		hwnd, (HMENU)NULL, NULL, NULL);
	sfmlwnd[3].create(hDisplay[3]);
	
	//StaticWalls
	hWall[0] = CreateWindow("Static",NULL,
		WS_CHILD | WS_VISIBLE | WS_BORDER,
		255, 50, 300, 10,
		hwnd, (HMENU)NULL, NULL, NULL);
	hWall[1] = CreateWindow("Static",NULL,
		WS_CHILD | WS_VISIBLE | WS_BORDER,
		245, 180, 10, 300,
		hwnd, (HMENU)NULL, NULL, NULL);
	hWall[2] = CreateWindow("Static",NULL,
		WS_CHILD | WS_VISIBLE | WS_BORDER,
		555, 180, 10, 300,
		hwnd, (HMENU)NULL, NULL, NULL);
	hWall[3] = CreateWindow("Static",NULL,
		WS_CHILD | WS_VISIBLE | WS_BORDER,
		255, 600, 300, 10,
		hwnd, (HMENU)NULL, NULL, NULL);
	
	hAnimationText = CreateWindow(
		"Static","��������",
		WS_CHILD | WS_VISIBLE,
		255, 3, 148, 45,
		hwnd, (HMENU)NULL, NULL, NULL);
	SendMessage(hAnimationText,WM_SETFONT,(WPARAM)bigFont,0);
	ShowWindow(hAnimationText, SW_SHOWNORMAL);	
	
	
	//Colors
	hRedEdit = CreateWindow("edit", "",
		WS_CHILD | WS_VISIBLE | WS_BORDER | ES_LEFT | ES_NUMBER, 390, 230, 30, 20,
		hwnd, 0, hInst, NULL);
	ShowWindow(hRedEdit, SW_SHOWNORMAL);
	SendMessage(hRedEdit,EM_SETLIMITTEXT,3,0);
	
	hGreenEdit = CreateWindow("edit", "",
		WS_CHILD | WS_VISIBLE | WS_BORDER | ES_LEFT | ES_NUMBER, 390, 275, 30, 20,
		hwnd, 0, hInst, NULL);
	ShowWindow(hGreenEdit, SW_SHOWNORMAL);
	SendMessage(hGreenEdit,EM_SETLIMITTEXT,3,0);
	
	hBlueEdit = CreateWindow("edit", "",
		WS_CHILD | WS_VISIBLE | WS_BORDER | ES_LEFT | ES_NUMBER, 390, 320, 30, 20,
		hwnd, 0, hInst, NULL);
	ShowWindow(hBlueEdit, SW_SHOWNORMAL);
	SendMessage(hBlueEdit,EM_SETLIMITTEXT,3,0);
	
	hRedText = CreateWindow(
		"Static","R",
		WS_CHILD | WS_VISIBLE,
		400, 210, 10, 15,
		hwnd, (HMENU)NULL, NULL, NULL);
	SendMessage(hRedText,WM_SETFONT,(WPARAM)font,0);
	ShowWindow(hRedText, SW_SHOWNORMAL);
	
	hGreenText = CreateWindow(
		"Static","G",
		WS_CHILD | WS_VISIBLE,
		400, 255, 10, 15,
		hwnd, (HMENU)NULL, NULL, NULL);
	SendMessage(hGreenText,WM_SETFONT,(WPARAM)font,0);
	ShowWindow(hGreenText, SW_SHOWNORMAL);
	
	hBlueText = CreateWindow(
		"Static","B",
		WS_CHILD | WS_VISIBLE,
		400, 300, 10, 15,
		hwnd, (HMENU)NULL, NULL, NULL);
	SendMessage(hBlueText,WM_SETFONT,(WPARAM)font,0);
	ShowWindow(hBlueText, SW_SHOWNORMAL);
	
	hColorText = CreateWindow(
		"Static","����",
		WS_CHILD | WS_VISIBLE,
		385, 180, 40, 20,
		hwnd, (HMENU)NULL, NULL, NULL);
	SendMessage(hColorText,WM_SETFONT,(WPARAM)font,0);
	ShowWindow(hColorText, SW_SHOWNORMAL);
	
	
	//Buttons
	hAnimateBtn = CreateWindow("button", "���",
		WS_CHILD | WS_VISIBLE | WS_BORDER,
		415, 10, 45, 35, hwnd, 0, hInst, NULL);
	ShowWindow(hAnimateBtn, SW_SHOWNORMAL);
	
	hWallpapersButton = CreateWindow("button", "�������",
		WS_CHILD | WS_VISIBLE | WS_BORDER,
		370, 350, 70, 30, hwnd, 0, hInst, NULL);
	ShowWindow(hWallpapersButton, SW_SHOWNORMAL);
	
	hRandomColorBtn = CreateWindow("button", "������",
		WS_CHILD | WS_VISIBLE | WS_BORDER,
		370, 410, 70, 30, hwnd, 0, hInst, NULL);
	ShowWindow(hRandomColorBtn, SW_SHOWNORMAL);
	
	hSaveBtn = CreateWindow("button", "Save",
		WS_CHILD | WS_VISIBLE | WS_BORDER | BS_ICON,
		10, 10, 35, 35, hwnd, 0, hInst, NULL);
	ShowWindow(hAnimateBtn, SW_SHOWNORMAL);
	
	hLoadBtn = CreateWindow("button", "Load",
		WS_CHILD | WS_VISIBLE | WS_BORDER | BS_ICON,
		60, 10, 35, 35, hwnd, 0, hInst, NULL);
	ShowWindow(hLoadBtn, SW_SHOWNORMAL);
	
	hThemeBtn = CreateWindow("button", "Theme",
		WS_CHILD | WS_VISIBLE | WS_BORDER | BS_ICON,
		110, 10, 35, 35, hwnd, 0, hInst, NULL);
	ShowWindow(hThemeBtn, SW_SHOWNORMAL);
	
	hPresetsBtn = CreateWindow("button", "�������",
		WS_CHILD | WS_VISIBLE | WS_BORDER,
		160, 65, 70, 30, hwnd, 0, hInst, NULL);
	ShowWindow(hPresetsBtn, SW_SHOWNORMAL);
	
	if (isFullVersion){
		hExitBtn = CreateWindow("button", "Exit",
			WS_CHILD | WS_VISIBLE | WS_BORDER | BS_ICON,
			160, 10, 35, 35, hwnd, 0, hInst, NULL);
		ShowWindow(hThemeBtn, SW_SHOWNORMAL);
	}
	
	hSumBtn = CreateWindow("button", "���������",
		WS_CHILD | WS_VISIBLE | WS_BORDER,
		48, 470, 144, 28, hwnd, 0, hInst, NULL);
	ShowWindow(hSumBtn, SW_SHOWNORMAL);
	SendMessage(hSumBtn,WM_SETFONT,(WPARAM)resultFont,0);
	
	hSaveResBtn = CreateWindow("button", "��������",
		WS_CHILD | WS_VISIBLE | WS_BORDER,
		48, 510, 110, 28, hwnd, 0, hInst, NULL);
	ShowWindow(hSaveResBtn, SW_SHOWNORMAL);
	SendMessage(hSaveResBtn,WM_SETFONT,(WPARAM)resultFont,0);
	
	hResetBtn = CreateWindow("button", "��������",
		WS_CHILD | WS_VISIBLE | WS_BORDER,
		48, 550, 90, 30, hwnd, 0, hInst, NULL);
	ShowWindow(hResetBtn, SW_SHOWNORMAL);
	SendMessage(hResetBtn,WM_SETFONT,(WPARAM)resultFont,0);
	
	hSaveSettsBtn = CreateWindow("button", "���������",
		WS_CHILD | WS_VISIBLE | WS_BORDER | BS_ICON,
		160, 510, 30, 28, hwnd, 0, hInst, NULL);
	ShowWindow(hSaveSettsBtn, SW_SHOWNORMAL);
	SendMessage(hSaveSettsBtn,WM_SETFONT,(WPARAM)resultFont,0);
	
	//Icons
	HANDLE hAnimateIcon,hSaveIcon,hLoadIcon,hThemeIcon,hExitIcon,hSettsIcon;
	//Save
	hSaveIcon = LoadImage(NULL,"res/icons/save.ico",IMAGE_ICON,22, 22,LR_LOADFROMFILE);
	SendMessage(hSaveBtn,BM_SETIMAGE, IMAGE_ICON, LPARAM (hSaveIcon));
	//load
	hLoadIcon = LoadImage(NULL,"res/icons/load.ico",IMAGE_ICON,22, 22,LR_LOADFROMFILE);
	SendMessage(hLoadBtn,BM_SETIMAGE, IMAGE_ICON, LPARAM (hLoadIcon));
	//Theme
	hThemeIcon = LoadImage(NULL,"res/icons/moon.ico",IMAGE_ICON,22, 22,LR_LOADFROMFILE);
	SendMessage(hThemeBtn,BM_SETIMAGE, IMAGE_ICON, LPARAM (hThemeIcon));
	//Settings
	hSettsIcon = LoadImage(NULL,"res/icons/settings.ico",IMAGE_ICON,22, 22,LR_LOADFROMFILE);
	SendMessage(hSaveSettsBtn,BM_SETIMAGE, IMAGE_ICON, LPARAM (hSettsIcon));
	//Exit
	if (isFullVersion){
		hExitIcon = LoadImage(NULL,"res/icons/exit.ico",IMAGE_ICON,0, 0,LR_LOADFROMFILE);
		SendMessage(hExitBtn,BM_SETIMAGE, IMAGE_ICON, LPARAM (hExitIcon));
	}
}

void destroyComponents(){
	DestroyWindow(hWallpapersEdit);
	DestroyWindow(hWallpapersEdit2);
	DestroyWindow(hWallEdit);
	DestroyWindow(hWallEdit2);
	DestroyWindow(hWallEdit3);
	DestroyWindow(hSumEdit);
	DestroyWindow(hWallpapersLength);
	DestroyWindow(hWallpapersHeight);
	DestroyWindow(hWallpapersText);
	DestroyWindow(hWallText);
	DestroyWindow(hWallLength);
	DestroyWindow(hWallHeight);
	DestroyWindow(hWallWidth);
	DestroyWindow(hSumText);
	DestroyWindow(hAnimateBtn);
	DestroyWindow(hSaveBtn);
	DestroyWindow(hLoadBtn);
	if (isFullVersion){
		DestroyWindow(hExitBtn);
	}
	DestroyWindow(hThemeBtn);
	DestroyWindow(hSumBtn);
	DestroyWindow(hRollsEdit);
	DestroyWindow(hPercentsText);
	DestroyWindow(hRollsText);
	DestroyWindow(hAnimationText);
	DestroyWindow(hPresetsBtn);
	DestroyWindow(hWallpapersButton);
	DestroyWindow(hRedText);
	DestroyWindow(hGreenText);
	DestroyWindow(hBlueText);
	DestroyWindow(hRedEdit);
	DestroyWindow(hGreenEdit);
	DestroyWindow(hBlueEdit);
	DestroyWindow(hColorText);
	DestroyWindow(hSaveResBtn);
	DestroyWindow(hSaveSettsBtn);
	DestroyWindow(hResetBtn);
	DestroyWindow(hRandomColorBtn);
	
	for (int i = 0; i < 4; i++){
		DestroyWindow(hDisplay[i]);
		DestroyWindow(hWall[i]);
	}
}

void renderLogInForm(HWND hwnd, HINSTANCE hInst){
	
	//Login
	hLoginEdit = CreateWindow("edit", "",
		WS_CHILD | WS_VISIBLE | WS_BORDER | ES_LEFT, 80, 280, 140, 20,
		hwnd, 0, hInst, NULL);
	ShowWindow(hLoginEdit, SW_SHOWNORMAL);

	hLogInPasswordEdit = CreateWindow("edit", "",
		WS_CHILD | WS_VISIBLE | WS_BORDER | ES_LEFT | ES_PASSWORD, 80, 330, 140, 20,
		hwnd, 0, hInst, NULL);
	ShowWindow(hLogInPasswordEdit, SW_SHOWNORMAL);
	
	hLogInBtn = CreateWindow("button", "�����",
		    WS_CHILD | WS_VISIBLE | WS_BORDER,
		    112, 375, 70, 28, hwnd, 0, hInst, NULL);
	ShowWindow(hLogInBtn, SW_SHOWNORMAL);
	SendMessage(hLogInBtn,WM_SETFONT,(WPARAM)resultFont,0);
	
	hLogInText = CreateWindow(
		"Static","����",
		WS_CHILD | WS_VISIBLE,
		112, 200, 73, 48,
		hwnd, (HMENU)NULL, NULL, NULL);
	SendMessage(hLogInText,WM_SETFONT,(WPARAM)bigFont,0);
	ShowWindow(hLogInText, SW_SHOWNORMAL);
	
	hLogInEditText = CreateWindow(
		"Static","�����",
		WS_CHILD | WS_VISIBLE,
		80, 260, 50, 20,
		hwnd, (HMENU)NULL, NULL, NULL);
	ShowWindow(hLogInEditText, SW_SHOWNORMAL);
	SendMessage(hLogInEditText,WM_SETFONT,(WPARAM)font,0);
	
	hLogInPasswordText = CreateWindow(
		"Static","������",
		WS_CHILD | WS_VISIBLE,
		80, 310, 60, 20,
		hwnd, (HMENU)NULL, NULL, NULL);
	ShowWindow(hLogInPasswordText, SW_SHOWNORMAL);
	SendMessage(hLogInPasswordText,WM_SETFONT,(WPARAM)font,0);
	
	
	//SignUp
	hSignUpEdit = CreateWindow("edit", "",
		WS_CHILD | WS_VISIBLE | WS_BORDER | ES_LEFT, 340, 280, 140, 20,
		hwnd, 0, hInst, NULL);
	ShowWindow(hSignUpEdit, SW_SHOWNORMAL);

	hSignUpPasswordEdit = CreateWindow("edit", "",
		WS_CHILD | WS_VISIBLE | WS_BORDER | ES_LEFT | ES_PASSWORD, 340, 330, 140, 20,
		hwnd, 0, hInst, NULL);
	ShowWindow(hSignUpPasswordEdit, SW_SHOWNORMAL);
	
	hSignUpBtn = CreateWindow("button", "������������������",
		    WS_CHILD | WS_VISIBLE | WS_BORDER,
		    315, 375, 190, 28, hwnd, 0, hInst, NULL);
	ShowWindow(hSignUpBtn, SW_SHOWNORMAL);
	SendMessage(hSignUpBtn,WM_SETFONT,(WPARAM)resultFont,0);
	
	hSignUpText = CreateWindow(
		"Static","�����������",
		WS_CHILD | WS_VISIBLE,
		320, 200, 180, 48,
		hwnd, (HMENU)NULL, NULL, NULL);
	SendMessage(hSignUpText,WM_SETFONT,(WPARAM)bigFont,0);
	ShowWindow(hSignUpText, SW_SHOWNORMAL);
	
	hSignUpEditText = CreateWindow(
		"Static","�����",
		WS_CHILD | WS_VISIBLE,
		340, 260, 50, 20,
		hwnd, (HMENU)NULL, NULL, NULL);
	ShowWindow(hSignUpEditText, SW_SHOWNORMAL);
	SendMessage(hSignUpEditText,WM_SETFONT,(WPARAM)font,0);
	
	hSignUpPasswordText = CreateWindow(
		"Static","������",
		WS_CHILD | WS_VISIBLE,
		340, 310, 60, 20,
		hwnd, (HMENU)NULL, NULL, NULL);
	ShowWindow(hSignUpPasswordText, SW_SHOWNORMAL);
	SendMessage(hSignUpPasswordText,WM_SETFONT,(WPARAM)font,0);
	
	hWelcomeText = CreateWindow(
		"Static","Welcome",
		WS_CHILD | WS_VISIBLE,
		210, 20, 130, 80,
		hwnd, (HMENU)NULL, NULL, NULL);
	ShowWindow(hWelcomeText, SW_SHOWNORMAL);
	SendMessage(hWelcomeText,WM_SETFONT,(WPARAM)welcomeFont,0);
}

void destroyLogInForm(HWND hwnd, HINSTANCE hInst){
	DestroyWindow(hLoginEdit);
	DestroyWindow(hSignUpEdit);
	DestroyWindow(hLogInPasswordEdit);
	DestroyWindow(hSignUpPasswordEdit);
	DestroyWindow(hLogInBtn);
	DestroyWindow(hSignUpBtn);
	DestroyWindow(hLogInText);
	DestroyWindow(hSignUpText);
	DestroyWindow(hWelcomeText);
	DestroyWindow(hLogInEditText);
	DestroyWindow(hSignUpEditText);
	DestroyWindow(hLogInPasswordText);
	DestroyWindow(hSignUpPasswordText);
}

void logIn(HWND hwnd, HINSTANCE hInst){
	char login[16], password[16];
	sqlite3_open("db/database.db",&db);
	const char* SQLquery="Select * From 'users' Where login = ? and password = ?;";
	char *wpL = "", *wpH = "", *wL = "", *wW = "", *wH = "";
	bool isFound = false;
				
	GetWindowText(hLoginEdit, login, 16);
	GetWindowText(hLogInPasswordEdit, password, 16);
	sqlite3_prepare(db, SQLquery, -1, &stmtU, 0);
	sqlite3_bind_text(stmtU, 1, login, -1,0);
	sqlite3_bind_text(stmtU, 2, password, -1,0);
	while (sqlite3_step(stmtU) == SQLITE_ROW){
		userLogin = "";
		userLogin = login;
		isFound = true;
		destroyLogInForm(hwnd, hInst);
		renderComponents(hwnd, hInst);
		takeColor();
	}
	sqlite3_reset(stmtU);
	sqlite3_finalize(stmtU);
	sqlite3_close(db);
	if (!isFound)
		MessageBox(NULL, "������������ ����� ��� ������","������!",MB_ICONEXCLAMATION|MB_OK);
}

void signUp(HWND hwnd, HINSTANCE hInst){
	char login[16], password[16];
	sqlite3_open("db/database.db",&db);
	const char* SQLquery="Select * From 'users' Where login = ?;";
	bool isFound = false;
				
	GetWindowText(hSignUpEdit, login, 16);
	GetWindowText(hSignUpPasswordEdit, password, 16);
	sqlite3_prepare(db, SQLquery, -1, &stmtU, 0);
	sqlite3_bind_text(stmtU, 1, login, -1,0);
	while (sqlite3_step(stmtU) == SQLITE_ROW){
		isFound = true;
		break;
	}
	sqlite3_reset(stmtU);
	sqlite3_finalize(stmtU);
	sqlite3_close(db);
	if (isFound){
		MessageBox(NULL, "������������ � ����� ������� ��� ����������","������!",MB_ICONEXCLAMATION|MB_OK);
	} else {
		sqlite3_open("db/database.db",&db);
		const char* SQLqueryInsert= "Insert Into users (login,password,wpLength,wpHeight,wLength,wWidth,wHeight,result,wpRolls) Values (?,?,NULL,NULL,NULL,NULL,NULL,NULL,NULL);";
		sqlite3_prepare(db, SQLqueryInsert, -1, &stmtU, 0);
		sqlite3_bind_text(stmtU, 1, login, -1,0);
		sqlite3_bind_text(stmtU, 2, password, -1,0);
		sqlite3_step(stmtU);
		sqlite3_reset(stmtU);
		sqlite3_finalize(stmtU);
		sqlite3_close(db);
		SetWindowText(hLoginEdit, login);
		SetWindowText(hLogInPasswordEdit, password);
	}
}

/* This is where all the input to the window goes to */
LRESULT CALLBACK WndProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam) {
	
	HINSTANCE hInst;

	switch(Message) {
		case WM_CREATE: {
			HMENU  hMenubar = CreateMenu();
			SetMenu(hwnd, hMenubar);
			hInst = ((LPCREATESTRUCT)lParam)->hInstance;
		    if (checkIsFullVersion()){
				isFullVersion = true;
				renderLogInForm(hwnd,hInst);
			} else {
				isFullVersion = false;
				renderComponents(hwnd, hInst);
				takeColor();
			}
		    changeTheme(hwnd,true);
		    
		    checkAnimate();
		    /*
		    char *err = 0;
			sqlite3_open("db/database.db",&db);
			const char* SQLquery= "Create table if not exists users(login CHAR(30),password CHAR(30),wpLength REAL(10),wpHeight REAL(10),wLength REAL(10),wWidth REAL(10),wHeight REAL(10),result INTEGER(11),wpRolls INTEGER(11));";
			sqlite3_prepare(db, SQLquery, -1, &stmtU, 0);
			sqlite3_step(stmtU);
			sqlite3_reset(stmtU);
			sqlite3_finalize(stmtU);
			const char* SQLqueryInsert= "Insert Into users (login,password,wpLength,wpHeight,wLength,wWidth,wHeight,result,wpRolls) Values ('user','0000',4,5,4,6,5,0,5);";
			sqlite3_prepare(db, SQLqueryInsert, -1, &stmtU, 0);
			sqlite3_step(stmtU);
			sqlite3_reset(stmtU);
			sqlite3_finalize(stmtU);
			sqlite3_close(db);*/
		    
			break;
		}
		
		case WM_CTLCOLOREDIT:{
			HDC hEdits = (HDC)wParam;
			if (!lightTheme){
			    SetTextColor(hEdits, RGB(255, 255, 255));
				SetBkColor(hEdits, RGB(53, 60, 55));
			} else {
				SetTextColor(hEdits, RGB(0, 0, 0));
				SetBkColor(hEdits, RGB(255, 255, 255));
			}
			
			return (INT_PTR)GetStockObject(WHITE_BRUSH);
			break;
		}
		
		case WM_CTLCOLORSTATIC:{
			HDC hStats = (HDC)wParam;
			if (!lightTheme){
				SetTextColor(hStats, RGB(255, 255, 255));
				SetBkColor(hStats, RGB(0, 0, 0));
			} else {
				SetTextColor(hStats, RGB(0, 0, 0));
				SetBkColor(hStats, RGB(255, 255, 255));
			}
			return (INT_PTR)GetStockObject(WHITE_BRUSH);
			break;
		}
	
		//Background Color
		case WM_PAINT:{
			PAINTSTRUCT ps;
			RECT rc;
			HDC hdc = BeginPaint(hwnd, &ps);
			GetClientRect(hwnd, &rc);
			if (!lightTheme){
			    SetDCBrushColor(hdc, RGB(0,0,0));
			} else {
				SetDCBrushColor(hdc, RGB(255,255,255));
			}
			FillRect(hdc, &rc, (HBRUSH)GetStockObject(DC_BRUSH));
			EndPaint(hwnd, &ps);
			for (int i = 0; i < 4; i++){
				//Displays
				hdc = BeginPaint(hDisplay[i], &ps);
				GetClientRect(hDisplay[i], &rc);
				if (!lightTheme){
				    SetDCBrushColor(hdc, RGB(53,60,55));
				} else {
					SetDCBrushColor(hdc, RGB(255,255,255));
				}
				FillRect(hdc, &rc, (HBRUSH)GetStockObject(DC_BRUSH));
				EndPaint(hDisplay[i], &ps);
				//Walls
				hdc = BeginPaint(hWall[i], &ps);
				GetClientRect(hWall[i], &rc);
				if (!lightTheme){
				    SetDCBrushColor(hdc, RGB(53,60,55));
				} else {
					SetDCBrushColor(hdc, RGB(255,255,255));
				}
				FillRect(hdc, &rc, (HBRUSH)GetStockObject(DC_BRUSH));
				EndPaint(hWall[i], &ps);
			}
			return 0;
		    break;
		}
		
		case WM_ERASEBKGND:
	    return 0;
		
		case WM_COMMAND:{
			
			if (lParam == (LPARAM)hRandomColorBtn){
				randomizeColor();
				break;
			}
			if (lParam == (LPARAM)hResetBtn){
				clearWindows();
				break;
			}
			if (lParam == (LPARAM)hSaveResBtn){
				saveResult();
				break;
			}
			if (lParam == (LPARAM)hSaveSettsBtn){
				browseFolder();
				break;
			}
			if (lParam == (LPARAM)hWallpapersButton){
				changeColor();
				break;
			}
			if (lParam == (LPARAM)hPresetsBtn){
				renderPreset();
				break;
			}
			if (lParam == (LPARAM)hExitBtn){
				destroyComponents();
				renderLogInForm(hwnd,hInst);
				break;
			}
			if (lParam == (LPARAM)hSignUpBtn){
				signUp(hwnd, hInst);
				break;
			}
			if (lParam == (LPARAM)hLoadBtn){
				if (isFullVersion){
					loadFromDB();
					break;
				} else {
					loadFromRegistry();
					break;
				}
			}
			if (lParam == (LPARAM)hSaveBtn){
				if (isFullVersion){
					saveValuesToDB();
					break;
				} else {
					saveValuesToRegistry();
					break;
				}
			}
			if (lParam == (LPARAM)hLogInBtn){
				logIn(hwnd, hInst);
				break;
			}
			if (lParam == (LPARAM)hSumBtn){
				isSaved = false;
				char wallpaperLengthArr[64],wallpaperHeightArr[32],
					wallLengthArr[32],wallHeightArr[32],wallWidthArr[32], *percent;
				bool isNumber = true;
				float remainder;
				wallpapersLength = wallpapersHeight = wallWidth = 
				wallLength = wallHeight = remainder = 0.0;
				
				//Wallpapers
				GetWindowText(hWallpapersEdit, wallpaperLengthArr, 32);
				if (!checkEdit(wallpaperLengthArr))
					break;
				
				GetWindowText(hWallpapersEdit2, wallpaperHeightArr, 32);
				if (!checkEdit(wallpaperHeightArr))
					break;
				
				//Walls
				GetWindowText(hWallEdit, wallLengthArr, 32);
				if (!checkEdit(wallLengthArr))
					break;
				
				GetWindowText(hWallEdit2, wallHeightArr, 32);
				if (!checkEdit(wallHeightArr))
					break;
				
				GetWindowText(hWallEdit3, wallWidthArr, 32);
				if (!checkEdit(wallWidthArr))
					break;
				
				if (setParameters(wallpaperLengthArr,wallpaperHeightArr,wallLengthArr,wallHeightArr,wallWidthArr)){
					if (isAnimate)
						animate();
				}
				break;
			}
			if (lParam == (LPARAM)hThemeBtn){
				changeTheme(hwnd,false);
				break;
			}
			if (lParam == (LPARAM)hAnimateBtn){
				changeAnimate();
				break;
			}
			break;
		}
		
		case WM_CLOSE:{
			if (!isSaved){
				int msgboxID = MessageBox(NULL,"������� ����������� ����� �������?","�� �� �����������",MB_ICONQUESTION | MB_YESNO);
			    if (msgboxID == IDYES){
			    	if (isFullVersion){
						saveValuesToDB();
					} else
						saveValuesToRegistry();
				}
				DeleteObject(font);
				DeleteObject(resultFont);
				DeleteObject(bigFont);
				sfmlwnd[0].close();
				sfmlwnd[1].close();
				sfmlwnd[2].close();
				sfmlwnd[3].close();
			}
			DestroyWindow(hwnd);
			break;
		}

		/* Upon destruction, tell the main thread to stop */
		case WM_DESTROY: {
			PostQuitMessage(0);
			break;
		}
		
		/* All other messages (a lot of them) are processed using default procedures */
		default:
			return DefWindowProc(hwnd, Message, wParam, lParam);
	}
	return 0;
}

/* The 'main' function of Win32 GUI programs: this is where execution starts */
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	WNDCLASSEX wc; /* A properties struct of our window */
	HWND hwnd; /* A 'HANDLE', hence the H, or a pointer to our window */
	MSG msg; /* A temporary location for all messages */

	/* zero out the struct and set the stuff we want to modify */
	memset(&wc,0,sizeof(wc));
	wc.cbSize		 = sizeof(WNDCLASSEX);
	wc.lpfnWndProc	 = WndProc; /* This is where we will send messages to */
	wc.hInstance	 = hInstance;
	wc.hCursor		 = LoadCursor(NULL, IDC_ARROW);
	
	/* White, COLOR_WINDOW is just a #define for a system color, try Ctrl+Clicking it */
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
	wc.lpszClassName = "WindowClass";
	wc.hIcon		 = LoadIcon(NULL, IDI_APPLICATION); /* Load a standard icon */
	wc.hIconSm		 = LoadIcon(NULL, IDI_APPLICATION); /* use the name "A" to use the project icon */

	if(!RegisterClassEx(&wc)) {
		MessageBox(NULL, "Window Registration Failed!","Error!",MB_ICONEXCLAMATION|MB_OK);
		return 0;
	}

	hwnd = CreateWindowEx(WS_EX_CLIENTEDGE,"WindowClass","Caption",WS_VISIBLE|WS_OVERLAPPED
		|WS_SYSMENU|WS_MINIMIZEBOX,
		CW_USEDEFAULT, /* x */
		CW_USEDEFAULT, /* y */
		600, /* width */
		650, /* height */
		NULL,NULL,hInstance,NULL);

	if(hwnd == NULL) {
		MessageBox(NULL, "Window Creation Failed!","Error!",MB_ICONEXCLAMATION|MB_OK);
		return 0;
	}

	/*
		This is the heart of our program where all input is processed and 
		sent to WndProc. Note that GetMessage blocks code flow until it receives something, so
		this loop will not produce unreasonably high CPU usage
	*/
	while(GetMessage(&msg, NULL, 0, 0) > 0) { /* If no error is received... */
		TranslateMessage(&msg); /* Translate key codes to chars if present */
		DispatchMessage(&msg); /* Send it to WndProc */
	}
	return msg.wParam;
}
